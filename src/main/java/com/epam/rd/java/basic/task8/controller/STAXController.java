package com.epam.rd.java.basic.task8.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.xml.sax.helpers.DefaultHandler;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Flower.VisualParameters;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private static final String TEMPRETURE_TAG = "tempreture";
	private static final String LIGHTING_TAG = "lighting";
	private static final String LIGHT_REQUIRING_TAG = "lightRequiring";
	private static final String WATERING_TAG = "watering";
	private static final String MULTIPLYING_TAG = "multiplying";
	private static final String MEASURE_TAG = "measure";
	private static final String AVE_LEN_FLOWER_TAG = "aveLenFlower";
	private static final String LEAF_COLOUR_TAG = "leafColour";
	private static final String STEM_COLOUR_TAG = "stemColour";
	private static final String GROWING_TIPS_TAG = "growingTips";
	private static final String VISUAL_PARAMETERS_TAG = "visualParameters";
	private static final String ORIGIN_TAG = "origin";
	private static final String SOIL_TAG = "soil";
	private static final String NAME_TAG = "name";
	private static final String FLOWER_TAG = "flower";
	private String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public List<Flower> parse() {
		List<Flower> result = new ArrayList<>();
		Flower flower = null;
		VisualParameters vp = null;
		GrowingTips gt = null;

		try {
			XMLEventReader xmlReader = XMLInputFactory.newInstance().createXMLEventReader(xmlFileName,
					new FileInputStream(xmlFileName));

			while (xmlReader.hasNext()) {
				XMLEvent event = xmlReader.nextEvent();

				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					String name = startElement.getName().getLocalPart();
					
					if(name.equals(FLOWER_TAG)) {
						flower = new Flower();
					} else if(name.equals(NAME_TAG)) {
						event = xmlReader.nextEvent();
						flower.setName(event.asCharacters().getData());
					} else if(name.equals(SOIL_TAG)) {
						event = xmlReader.nextEvent();
						flower.setSoil(event.asCharacters().getData());
					} else if(name.equals(ORIGIN_TAG)) {
						event = xmlReader.nextEvent();
						flower.setOrigin(event.asCharacters().getData());
					} else if(name.equals(VISUAL_PARAMETERS_TAG)) {
						vp = new VisualParameters();
					} else if(name.equals(GROWING_TIPS_TAG)) {
						gt = new GrowingTips();
					} else if(name.equals(STEM_COLOUR_TAG)) {
						event = xmlReader.nextEvent();
						vp.setStemColor(event.asCharacters().getData());
					} else if(name.equals(LEAF_COLOUR_TAG)) {
						event = xmlReader.nextEvent();
						vp.setLeafColour(event.asCharacters().getData());
					} else if(name.equals(AVE_LEN_FLOWER_TAG)) {
						event = xmlReader.nextEvent();
						vp.setAveLenFlower(event.asCharacters().getData());
						Attribute attr = startElement.getAttributeByName(new QName(MEASURE_TAG));
						vp.setAveLenFlowerMeasureAttribute(attr.getValue());
					} else if(name.equals(TEMPRETURE_TAG)) {
						event = xmlReader.nextEvent();
						gt.setTemperature(Integer.parseInt(event.asCharacters().getData()));
						Attribute attr = startElement.getAttributeByName(new QName(MEASURE_TAG));
						gt.setTemperatureMeasure(attr.getValue());
					} else if(name.equals(LIGHTING_TAG)) {
						Attribute attr = startElement.getAttributeByName(new QName(LIGHT_REQUIRING_TAG));
						gt.setLightRequiring(attr.getValue().equals("yes"));
					} else if(name.equals(WATERING_TAG)) {
						event = xmlReader.nextEvent();
						gt.setWatering(Integer.parseInt(event.asCharacters().getData()));
						Attribute attr = startElement.getAttributeByName(new QName(MEASURE_TAG));
						gt.setWateringMeasure(attr.getValue());
					} else if(name.equals(MULTIPLYING_TAG)) {
						event = xmlReader.nextEvent();
						flower.setMultiplying(event.asCharacters().getData());
					}
				} else if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					String name = endElement.getName().getLocalPart();
					if(name.equals(FLOWER_TAG)) {
						result.add(flower);
						flower = null;
					} else if(name.equals(VISUAL_PARAMETERS_TAG)) {
						flower.setVisual(vp);
						vp = null;
					} else if(name.equals(GROWING_TIPS_TAG)) {
						flower.setTipsToGrow(gt);
						gt = null;
					}
				}
			}

		} catch (FileNotFoundException | XMLStreamException | FactoryConfigurationError e) {
			e.printStackTrace();
		}

		return result;
	}

}