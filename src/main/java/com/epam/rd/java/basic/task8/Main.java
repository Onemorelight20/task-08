package com.epam.rd.java.basic.task8;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Comparator;
import java.util.List;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flower;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		FromListToXMLParser fltx = new FromListToXMLParser();
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		
		DOMController domController = new DOMController(xmlFileName);
		List<Flower> flowers = domController.getDocumentFlowerItems();
		flowers.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));

		String xmlResult = fltx.parseListOfFlowersToXML(flowers);
	
		String outputXmlFile = "output.dom.xml";
		BufferedWriter buffWriter = new BufferedWriter(new FileWriter(new File(outputXmlFile)));
		buffWriter.append(xmlResult);
		buffWriter.close();
		
		
		
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		
		SAXController saxController = new SAXController(xmlFileName);
		List<Flower> flowersSAX = saxController.parse();
		flowersSAX.sort((o1, o2) -> o1.getOrigin().compareTo(o2.getOrigin()));
		
		String XMLFromSAX = fltx.parseListOfFlowersToXML(flowersSAX);
		
		outputXmlFile = "output.sax.xml";
		BufferedWriter buffWriter1 = new BufferedWriter(new FileWriter(new File(outputXmlFile)));
		buffWriter1.append(XMLFromSAX);
		buffWriter1.close();
		
		
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		List<Flower> flowersSTAX = staxController.parse();
		System.out.println(flowersSTAX);
		
		flowersSTAX.sort((o1, o2) -> o1.getSoil().compareTo(o2.getSoil()));
		String XMLFromSTAX = fltx.parseListOfFlowersToXML(flowersSAX);
		
		outputXmlFile = "output.stax.xml";
		BufferedWriter buffWriter2 = new BufferedWriter(new FileWriter(new File(outputXmlFile)));
		buffWriter2.append(XMLFromSTAX);
		buffWriter2.close();
		
	}

}
