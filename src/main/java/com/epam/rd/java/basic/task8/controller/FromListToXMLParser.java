package com.epam.rd.java.basic.task8.controller;

import java.util.List;

import com.epam.rd.java.basic.task8.entity.Flower;

public class FromListToXMLParser {
	
	public String parseObjectToFlowerXML(Flower flower) {
		StringBuilder result = new StringBuilder();
		result.append("<flower>\n");
		result.append(String.format("<name>%s</name>%n", flower.getName()));
		result.append(String.format("<soil>%s</soil>%n", flower.getSoil()));
		result.append(String.format("<origin>%s</origin>%n", flower.getOrigin()));
		result.append("<visualParameters>\n");
		result.append(String.format("<stemColour>%s</stemColour>\n", flower.getVisual().getStemColor()));
		result.append(String.format("<leafColour>%s</leafColour>\n", flower.getVisual().getLeafColour()));
		result.append(String.format("<aveLenFlower measure=\"%s\">%s</aveLenFlower>\n", 
				flower.getVisual().getAveLenFlowerMeasureAttribute(), flower.getVisual().getAveLenFlower()));
		result.append("</visualParameters>\n");
		result.append("<growingTips>\n");
		result.append(String.format("<tempreture measure=\"%s\">%d</tempreture>\n", 
				flower.getTipsToGrow().getTemperatureMeasure(), flower.getTipsToGrow().getTemperature()));
		result.append(String.format("<lighting lightRequiring=\"%s\"/>\n", 
				flower.getTipsToGrow().isLightRequiring() ? "yes" : "no"));
		result.append(String.format("<watering measure=\"%s\">%d</watering>\n", 
				flower.getTipsToGrow().getWateringMeasure(), flower.getTipsToGrow().getWatering()));
		result.append("</growingTips>\n");
		result.append(String.format("<multiplying>%s</multiplying>\n", flower.getMultiplying()));
		result.append("</flower>\n");
		return result.toString();
	}
	
	public String parseListOfFlowersToXML(List<Flower> list) {
		StringBuilder result = new StringBuilder();
		result.append("<flowers xmlns=\"http://www.nure.ua\"\r\n"
				+ "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
				+ "xsi:schemaLocation=\"http://www.nure.ua input.xsd \">\n");
		
		list.forEach(fl -> result.append(parseObjectToFlowerXML(fl)));
		result.append("</flowers>");
		return result.toString();
	}
}
