package com.epam.rd.java.basic.task8.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Flower.VisualParameters;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private static final String TEMPRETURE_TAG = "tempreture";
	private static final String LIGHTING_TAG = "lighting";
	private static final String LIGHT_REQUIRING_TAG = "lightRequiring";
	private static final String WATERING_TAG = "watering";
	private static final String MULTIPLYING_TAG = "multiplying";
	private static final String MEASURE_TAG = "measure";
	private static final String AVE_LEN_FLOWER_TAG = "aveLenFlower";
	private static final String LEAF_COLOUR_TAG = "leafColour";
	private static final String STEM_COLOUR_TAG = "stemColour";
	private static final String GROWING_TIPS_TAG = "growingTips";
	private static final String VISUAL_PARAMETERS_TAG = "visualParameters";
	private static final String ORIGIN_TAG = "origin";
	private static final String SOIL_TAG = "soil";
	private static final String NAME_TAG = "name";
	private static final String FLOWER_TAG = "flower";

	private String xmlFileName;
	private DocumentBuilderFactory factory;
	private DocumentBuilder builder;
	private Document document;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		try {
			init();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void init() throws ParserConfigurationException, SAXException, IOException {
		factory = DocumentBuilderFactory.newInstance();
		builder = factory.newDocumentBuilder();
		document = builder.parse(new File(xmlFileName));
		document.getDocumentElement().normalize();
	}

	public List<Flower> getDocumentFlowerItems() {
		NodeList flowersNodes = document.getElementsByTagName(FLOWER_TAG);
		List<Flower> result = new ArrayList<>();

		for (int i = 0; i < flowersNodes.getLength(); i++) {
			Node flowerNode = flowersNodes.item(i);
			Flower flower = new Flower();

			if (flowerNode.getNodeType() == Node.ELEMENT_NODE) {
				Element flowerElement = (Element) flowerNode;
				NodeList flowerDetails = flowerNode.getChildNodes();
				for (int j = 0; j < flowerDetails.getLength(); j++) {
					Node detail = flowerDetails.item(j);
					if (detail.getNodeType() == Node.ELEMENT_NODE) {

						Element detailElement = (Element) detail;
						if (detailElement.getTagName().equals(NAME_TAG)) {
							flower.setName(detailElement.getTextContent());
				} else if (detailElement.getTagName().equals(SOIL_TAG)) {
							flower.setSoil(detailElement.getTextContent());
						} else if (detailElement.getTagName().equals(ORIGIN_TAG)) {
							flower.setOrigin(detailElement.getTextContent());
						} else if (detailElement.getTagName().equals(VISUAL_PARAMETERS_TAG)) {
							VisualParameters vp = new VisualParameters();
							NodeList visualParametersDetails = detail.getChildNodes();
							for (int k = 0; k < visualParametersDetails.getLength(); k++) {
								Node vpDetail = visualParametersDetails.item(k);
								if (vpDetail.getNodeType() == Node.ELEMENT_NODE) {
									Element vpElement = (Element) vpDetail;
									if (vpElement.getTagName().equals(STEM_COLOUR_TAG)) {
										vp.setStemColor(vpElement.getTextContent());
									} else if (vpElement.getTagName().equals(LEAF_COLOUR_TAG)) {
										vp.setLeafColour(vpElement.getTextContent());
									} else if (vpElement.getTagName().equals(AVE_LEN_FLOWER_TAG)) {
										vp.setAveLenFlower(vpElement.getTextContent());
										vp.setAveLenFlowerMeasureAttribute(vpElement.getAttribute(MEASURE_TAG));
									}
								}
							}
							flower.setVisual(vp);
						} else if (detailElement.getTagName().equals(GROWING_TIPS_TAG)) {
							GrowingTips gt = new GrowingTips();
							NodeList growingTipsDetails = detail.getChildNodes();
							for (int f = 0; f < growingTipsDetails.getLength(); f++) {
								Node gtDetail = growingTipsDetails.item(f);
								if (gtDetail.getNodeType() == Node.ELEMENT_NODE) {
									Element gtElement = (Element) gtDetail;
									if (gtElement.getTagName().equals(TEMPRETURE_TAG)) {
										gt.setTemperature(Integer.parseInt(gtElement.getTextContent()));
										gt.setTemperatureMeasure(gtElement.getAttribute(MEASURE_TAG));
									} else if (gtElement.getTagName().equals(LIGHTING_TAG)) {
										boolean isRequired = false;
										if (gtElement.getAttribute(LIGHT_REQUIRING_TAG).equals("yes")) {
											isRequired = true;
										}
										gt.setLightRequiring(isRequired);
									} else if (gtElement.getTagName().equals(WATERING_TAG)) {
										gt.setWatering(Integer.parseInt(gtElement.getTextContent()));
										gt.setWateringMeasure(gtElement.getAttribute(MEASURE_TAG));
									}
								}
							}

							flower.setTipsToGrow(gt);
						} else if (detailElement.getTagName().equals(MULTIPLYING_TAG)) {
							flower.setMultiplying(detailElement.getTextContent());
						}

					}
				}
			}

			result.add(flower);
		}
		return result;
	}

}
