package com.epam.rd.java.basic.task8.entity;

public class Flower {
	private String name;
	private String soil;
	private String origin;
	private String multiplying;
	
	
	private VisualParameters visual;
	private GrowingTips tipsToGrow;
	
	public Flower() {
	}
	
	public Flower(String name, String soil, String origin, String multiplying, VisualParameters visual,
			GrowingTips tipsToGrow) {
		this.name = name;
		this.soil = soil;
		this.origin = origin;
		this.multiplying = multiplying;
		this.visual = visual;
		this.tipsToGrow = tipsToGrow;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSoil() {
		return soil;
	}

	public void setSoil(String soil) {
		this.soil = soil;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getMultiplying() {
		return multiplying;
	}

	public void setMultiplying(String multiplying) {
		this.multiplying = multiplying;
	}

	public VisualParameters getVisual() {
		return visual;
	}

	public void setVisual(VisualParameters visual) {
		this.visual = visual;
	}

	public GrowingTips getTipsToGrow() {
		return tipsToGrow;
	}

	public void setTipsToGrow(GrowingTips tipsToGrow) {
		this.tipsToGrow = tipsToGrow;
	}

	public static class GrowingTips{
		int temperature;
		String temperatureMeasure;
	
		boolean lightRequiring;
		
		int watering;
		String wateringMeasure;
		
		public GrowingTips(int temperature, String temperatureMeasure, boolean lightRequiring, int watering,
				String wateringMeasure) {
			this.temperature = temperature;
			this.temperatureMeasure = temperatureMeasure;
			this.lightRequiring = lightRequiring;
			this.watering = watering;
			this.wateringMeasure = wateringMeasure;
		}
		
		public GrowingTips() {
			
		}

		public int getTemperature() {
			return temperature;
		}

		public void setTemperature(int temperature) {
			this.temperature = temperature;
		}

		public String getTemperatureMeasure() {
			return temperatureMeasure;
		}

		public void setTemperatureMeasure(String temperatureMeasure) {
			this.temperatureMeasure = temperatureMeasure;
		}

		public boolean isLightRequiring() {
			return lightRequiring;
		}

		public void setLightRequiring(boolean lightRequiring) {
			this.lightRequiring = lightRequiring;
		}

		public int getWatering() {
			return watering;
		}

		public void setWatering(int watering) {
			this.watering = watering;
		}

		public String getWateringMeasure() {
			return wateringMeasure;
		}

		public void setWateringMeasure(String wateringMeasure) {
			this.wateringMeasure = wateringMeasure;
		}

		@Override
		public String toString() {
			return "GrowingTips [temperature=" + temperature + ", temperatureMeasure=" + temperatureMeasure
					+ ", lightRequiring=" + lightRequiring + ", watering=" + watering + ", wateringMeasure="
					+ wateringMeasure + "]";
		}
		
		
	}
	
	public static class VisualParameters{
		private String stemColor;
		private String leafColour;
		private String aveLenFlower;
		private String aveLenFlowerMeasureAttribute;
		
		public VisualParameters() {
			
		}
		public String getStemColor() {
			return stemColor;
		}
		public void setStemColor(String stemColor) {
			this.stemColor = stemColor;
		}
		public String getLeafColour() {
			return leafColour;
		}
		public void setLeafColour(String leafColour) {
			this.leafColour = leafColour;
		}
		public String getAveLenFlower() {
			return aveLenFlower;
		}
		public void setAveLenFlower(String aveLenFlower) {
			this.aveLenFlower = aveLenFlower;
		}
		public String getAveLenFlowerMeasureAttribute() {
			return aveLenFlowerMeasureAttribute;
		}
		public void setAveLenFlowerMeasureAttribute(String aveLenFlowerMeasureAttribute) {
			this.aveLenFlowerMeasureAttribute = aveLenFlowerMeasureAttribute;
		}
		public VisualParameters(String stemColor, String leafColour, String aveLenFlower,
				String aveLenFlowerMeasureAttribute) {
			this.stemColor = stemColor;
			this.leafColour = leafColour;
			this.aveLenFlower = aveLenFlower;
			this.aveLenFlowerMeasureAttribute = aveLenFlowerMeasureAttribute;
		}
		@Override
		public String toString() {
			return "VisualParameters [stemColor=" + stemColor + ", leafColour=" + leafColour + ", aveLenFlower="
					+ aveLenFlower + ", aveLenFlowerMeasureAttribute=" + aveLenFlowerMeasureAttribute + "]";
		}
		
		
	}

	@Override
	public String toString() {
		return "Flower [name=" + name + ", soil=" + soil + ", origin=" + origin + ", multiplying=" + multiplying
				+ ", visual=" + visual + ", tipsToGrow=" + tipsToGrow + "]";
	}
	
	
}
