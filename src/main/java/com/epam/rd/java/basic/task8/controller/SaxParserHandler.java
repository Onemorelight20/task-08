package com.epam.rd.java.basic.task8.controller;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flower.GrowingTips;
import com.epam.rd.java.basic.task8.entity.Flower.VisualParameters;

public class SaxParserHandler extends DefaultHandler{
	
	private static final String TEMPRETURE_TAG = "tempreture";
	private static final String LIGHTING_TAG = "lighting";
	private static final String LIGHT_REQUIRING_TAG = "lightRequiring";
	private static final String WATERING_TAG = "watering";
	private static final String MULTIPLYING_TAG = "multiplying";
	private static final String MEASURE_TAG = "measure";
	private static final String AVE_LEN_FLOWER_TAG = "aveLenFlower";
	private static final String LEAF_COLOUR_TAG = "leafColour";
	private static final String STEM_COLOUR_TAG = "stemColour";
	private static final String GROWING_TIPS_TAG = "growingTips";
	private static final String VISUAL_PARAMETERS_TAG = "visualParameters";
	private static final String ORIGIN_TAG = "origin";
	private static final String SOIL_TAG = "soil";
	private static final String NAME_TAG = "name";
	private static final String FLOWER_TAG = "flower";
	
	private List<Flower> flowers = null;
	private String currentElement = null;
	private Flower flower = null;
	private VisualParameters vp = null;
	private GrowingTips gt = null;
	
	@Override
	public void startDocument() throws SAXException {
		flowers = new ArrayList<>(); 	
	}
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentElement = qName;
		if(currentElement.equals(FLOWER_TAG)) {
			flower = new Flower();
		} else if(currentElement.equals(VISUAL_PARAMETERS_TAG)) {
			vp = new VisualParameters();
		} else if(currentElement.equals(GROWING_TIPS_TAG)) {
			gt = new GrowingTips();
		} else if(currentElement.equals(AVE_LEN_FLOWER_TAG)) {
			vp.setAveLenFlowerMeasureAttribute(attributes.getValue(MEASURE_TAG));
		} else if(currentElement.equals(TEMPRETURE_TAG)) {
			gt.setTemperatureMeasure(attributes.getValue(MEASURE_TAG));
		} else if(currentElement.equals(LIGHTING_TAG)) {
			gt.setLightRequiring(attributes.getValue(LIGHT_REQUIRING_TAG).equals("yes"));
		} else if(currentElement.equals(WATERING_TAG)) {
			gt.setWateringMeasure(attributes.getValue(MEASURE_TAG));
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		currentElement = null;
		if(qName.equals(FLOWER_TAG)) {
			flowers.add(flower);
			flower = null;
		} else if(qName.equals(VISUAL_PARAMETERS_TAG)) {
			flower.setVisual(vp);
			vp = null;
		} else if(qName.equals(GROWING_TIPS_TAG)) {
			flower.setTipsToGrow(gt);
			gt = null;
		}
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		String data = new String(ch, start, length).strip();
		
		if(currentElement == null) return;
		
		if(currentElement.equals(NAME_TAG)) {
			flower.setName(data);
		} else if(currentElement.equals(SOIL_TAG)) {
			flower.setSoil(data);
		} else if(currentElement.equals(ORIGIN_TAG)) {
			flower.setOrigin(data);
		} else if(currentElement.equals(STEM_COLOUR_TAG)) {
			vp.setStemColor(data);
		} else if(currentElement.equals(LEAF_COLOUR_TAG)) {
			vp.setLeafColour(data);
		} else if(currentElement.equals(AVE_LEN_FLOWER_TAG)) {
			vp.setAveLenFlower(data);
		} else if(currentElement.equals(TEMPRETURE_TAG)) {
			gt.setTemperature(Integer.valueOf(data));
		} else if(currentElement.equals(WATERING_TAG)) {
			gt.setWatering(Integer.valueOf(data));
		} else if(currentElement.equals(MULTIPLYING_TAG)) {
			flower.setMultiplying(data);
		}
	}
	
	public List<Flower> getResult(){
		return flowers;
	}
}
